import grpc.remotefs.routeguide.Message;
import grpc.remotefs.routeguide.Responce;
import grpc.remotefs.routeguide.RouteGuideGrpc;
import io.grpc.ManagedChannel;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class MessageServerTest {

    private static final String SERVER_NAME = "testServer";
    private MessageServer server;
    private ManagedChannel inProcessChannel;
    private Collection<Message> messages;

    @Before
    public void setUp() throws Exception {
        messages = new ArrayList<>();
        server = new MessageServer(InProcessServerBuilder.forName(SERVER_NAME).directExecutor());
        inProcessChannel = InProcessChannelBuilder.forName(SERVER_NAME).directExecutor().build();
        server.start();
    }

    @After
    public void tearDown() throws Exception {
        inProcessChannel.shutdownNow();
        server.stop();
    }

    @Test
    public void testRouteIsOpen() {
        Message m1 = Message.newBuilder().setMessage("test1").setTimestamp(System.currentTimeMillis()).build();

        @SuppressWarnings("unchecked")
        StreamObserver<Responce> responseObserver = (StreamObserver<Responce>) mock(StreamObserver.class);
        RouteGuideGrpc.RouteGuideStub stub = RouteGuideGrpc.newStub(inProcessChannel);

        StreamObserver<Message> requestObserver = stub.messageRoute(responseObserver);
        verify(responseObserver, never()).onNext(any(Responce.class));

        requestObserver.onNext(m1);
        verify(responseObserver, times(1)).onNext(any(Responce.class));
    }

}
