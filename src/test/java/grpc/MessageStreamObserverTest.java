package grpc;

import grpc.remotefs.routeguide.Message;
import grpc.remotefs.routeguide.Responce;
import io.grpc.stub.StreamObserver;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MessageStreamObserverTest {

    private MessageStreamObserver streamObserver;
    private StreamObserver mockResponceObserver;

    @Before
    public void setUp() throws Exception {
        mockResponceObserver = mock(StreamObserver.class);
        streamObserver = new MessageStreamObserver(mockResponceObserver);
    }

    @Test
    public void onNext() throws Exception {
        ArgumentCaptor<Responce> captor = ArgumentCaptor.forClass(Responce.class);
        Message m1 = Message.newBuilder().setMessage("test1").setTimestamp(System.currentTimeMillis()).build();
        streamObserver.onNext(m1);
        verify(mockResponceObserver, times(1)).onNext(captor.capture());
        assertEquals("Hash is computed correctly", m1.hashCode(), captor.getValue().getHash());
    }

}
