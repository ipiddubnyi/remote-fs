package grpc;

import grpc.remotefs.routeguide.Message;
import grpc.remotefs.routeguide.Responce;
import io.grpc.stub.StreamObserver;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageStreamObserver implements StreamObserver<Message> {

    private static final Logger LOGGER = Logger.getLogger(MessageStreamObserver.class.getName());

    private final StreamObserver<Responce> responseObserver;

    public MessageStreamObserver(StreamObserver<Responce> responseObserver) {
        this.responseObserver = responseObserver;
    }

    @Override
    public void onNext(Message message) {
        responseObserver.onNext(Responce.newBuilder().setHash(message.hashCode()).build());
    }

    @Override
    public void onError(Throwable throwable) {
        LOGGER.log(Level.ALL, "Error in observer", throwable);
    }

    @Override
    public void onCompleted() {
        LOGGER.log(Level.INFO, "Stream processing done");
    }
}
