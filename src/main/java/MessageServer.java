import grpc.MessageStreamObserver;
import grpc.remotefs.routeguide.Message;
import grpc.remotefs.routeguide.Responce;
import grpc.remotefs.routeguide.RouteGuideGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.logging.Logger;

public class MessageServer {
    private static final Logger LOGGER = Logger.getLogger(MessageServer.class.getName());
    private static final RouteGuideGrpc.RouteGuideImplBase SERVICE = new RouteGuideGrpc.RouteGuideImplBase() {
        @Override
        public StreamObserver<Message> messageRoute(StreamObserver<Responce> responseObserver) {
            return new MessageStreamObserver(responseObserver);
        }
    };
    private final Server server;

    public MessageServer(int port) throws IOException {
        this(ServerBuilder.forPort(port));
    }

    protected MessageServer(ServerBuilder<?> serverBuilder) throws IOException {
        this.server = serverBuilder.addService(SERVICE).build();
    }

    public void start() throws IOException {
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(MessageServer.this::stop));
        LOGGER.info("Server started");
    }

    public void stop() {
        LOGGER.info("Stopping server");
        if (server != null) {
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws Exception {
        MessageServer server = new MessageServer(4242);
        server.start();
        server.blockUntilShutdown();
    }
}
